//import processing.pdf.*;
// JavaScript

int numnodes = 600;
float mindistance = 10;
int min_node_size = 5;
float budget_factor = 0.18;
int min_budget = 80;
ArrayList network = new ArrayList();
Node selNode;

int xmargin = 10;
int ymargin = 10;
int minstroke = 1; // min stroke weight

boolean writepdf;

void setup(){
  size(570,400);
  background(255);
  //noStroke();
  stroke(0);
  //noFill();
  fill(255);
  build();
  smooth();
  frameRate(25);
}



void draw(){
  boolean rerender = false;
  for (Node N : network){
      if (N.reconnect()) rerender = true;
  }
  if (rerender) renderNodes();
       
}


void renderNodes(){
  background(255);

  translate(xmargin,ymargin);
  for (Node N : network){ // draw lines
    if (N.child == null) continue;
    strokeWeight(minstroke + sqrt((min(N.parents.size(),N.child.parents.size()))));
    line(N.pos.x,N.pos.y,N.child.pos.x,N.child.pos.y);
  }
 
  for (Node N : network){ // draw nodes
    fill(255);
    //if () continue;
    //if (N.parents.size() == 0) fill(0);
    if (N.child != null && N.pos.dist(N.child.pos) < (min_node_size + N.child.parents.size())/2) continue; // don't draw nodes too close to their child
    if (selNode != null && selNode == N && writepdf == false) fill(255,0,0);
    strokeWeight(minstroke + sqrt(N.parents.size())/2);
    ellipse (N.pos.x,N.pos.y,min_node_size + (N.parents.size()),min_node_size + (N.parents.size()));
  }

}

void build(){
  while (network.size() < numnodes){
    PVector newpos = new PVector(random(width-(xmargin*2)),random(height-(ymargin*2)));
    boolean addnode = true;
    for (int i=0; i<network.size(); i++){
      Node N = network.get(i);
      if (N.pos.dist(newpos) < mindistance){
        addnode = false;
        break;
      }
    }
    if (addnode) {
      Node newNode = new Node(newpos,(min_budget + width - newpos.x)*budget_factor);
     // println("budget: " + sq(width - newpos.x)); 
      network.add(newNode);
    }
  }
}


void keyPressed(){
  println(keyCode);
  if (key == 's'){
    //save("network_study_"+millis()+".png");
   // writepdf = true;
  }
  
  if (selNode != null){
    if (keyCode == UP){
      selNode.pos.y -= 1;
    }
    
    if (keyCode == DOWN){
      selNode.pos.y += 1;
    }
    
    if (keyCode == LEFT){
      selNode.pos.x -= 1;
    }
    
    if (keyCode == RIGHT){
      selNode.pos.x += 1;
    }
  
    if (keyCode == 8){
      if (selNode.child != null){
        selNode.child.parents.remove(selNode);
      }
      
      if (selNode.parents != null){
        for (Node N : selNode.parents){
          if (N.child == selNode) N.child = null;
        }
      }
     // selNode.child = null;
      network.remove(selNode);
      
    }
  
  }



}

void mouseReleased(){
 PVector mpos = new PVector (mouseX-xmargin,mouseY-ymargin);
 for (Node N: network){
   if (PVector.dist(mpos,N.pos) < 10){
     selNode = N;
     return;
   }
 }
 selNode = null;
}

void mouseDragged(){
  if (selNode == null) return;
   PVector mpos = new PVector (mouseX-xmargin,mouseY-ymargin);
   selNode.pos = mpos;
}

class Node {


  PVector pos;
  ArrayList<Node> parents = new ArrayList<Node>();
  Node child = null; 
  float budget;

  Node(PVector _pos, float _budget) {
    pos = _pos;
    budget = _budget;
  }

boolean reconnect() {
  // float currentcost = cost(child);
  // float currentcost = 1.0/child.parents.size();

  if (child == null) {
    int r = int(random(network.size()));
    Node newchild = network.get(r);
    if (newchild != this && pos.dist(newchild.pos) < budget){ 
      child = newchild;
      child.parents.add(this);
      return true;
    }
   // println(cost(newchild));
    return false;
  }

  int r = int(random(network.size()));
  Node newchild = network.get(r);
  if (newchild == this) return false;
  
  if (newchild.child == this) return false; // can't have a two-way connecntion

  if(pos.dist(newchild.pos) < budget && cost(newchild) < cost(child) && newchild.child != this ) {
    child.parents.remove(this);
    child = newchild;
    child.parents.add(this);
    return true;
  }
}


float cost(Node N) {
  return pow(pos.dist(N.pos),2)/ (N.parents.size()); /// distance squared over links
  // return pos.dist(N.pos) / (N.parents.size());
  // return pow(pos.dist(N.pos),2) / (N.parents.size() + N.grandParents());
}

int grandParents(){
  int gpcount = 0;
  
  for (Node G : parents){
    gpcount += G.parents.size();
  }
  
  return gpcount;
}

}




