##########################
# Creative Coding
# Cap 1
# 
# Rellena la pantalla de rectangulos aleatorios
# ¿De quien es esta obra?
# Basado en el código de la Universidad de Monarch
###########################

# setup -- Se ejecuta una sola vez al principio del programa
def setup():
    # Abrimos una ventana de 500 x 500 puntos
    size(500, 500)
    # Seleccionamos el fondo a blanco
    background(255)
    
    # Se selecciona el centro del rectangulo como origen
    rectMode(RADIUS)
    # El color del borde será gris claro
    stroke(170)
    # El color del rectangulo será negro, pero con transparencia
    fill(0, 150)
    


# draw  -- Se ejecuta en bucle hasta que se termina la ejecución del programa
def draw():
    rect(random(width), random(height), random(8), random(8))

    # Para borrar la pantalla pulsa r
    if keyPressed == True and key == 'r':
        background(255)
        
    # Para guardar el tranajo pulsa s
    if keyPressed == True and key=='s':
        saveFrame("capitulo1.jpg");
  

