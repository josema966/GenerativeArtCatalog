 ##############
 # Creative Coding
 #
 # En este programa seleccionamos un numero de cuadrados y un espacio aleatorio entre ellos. 
 # Desde aqui se calcula el lado de los cuadrados y se dibujan en pantalla utilizando dos bucles anidados
 # Tambien dibujamos una sombra para darle profundidad.
 # Inspirado en la obra de Vera Molnar. Basado en codigo de la Universidad de Monarch.
 # 
 #########################

def setup(): 
  size(600, 600)
  rectMode(CORNER)
  # En este caso el origen del cuadrado está en la esquina superior izquierda
  noStroke()
  frameRate(0.5)  
  # Con el frameRate le indicamos que queremos dos fotogramas por segundo



def draw():

  background(180); 
  # Limpia la pantalla pintandola de gris
  
  num = random(3, 12)
  # selecciona el numero de cuadrados por lado
  gap = random(5, 50)
  # seleciona la separacion de los cuadrados
  
  # calcula el lado de los cuadrados
  cellsize = ( width - (num + 1) * gap ) / num
  
  
  # calcula donde irá la sombra
  offsetX = cellsize/16.0
  offsetY = cellsize/16.0
  
  # Empiezan las iteraciones
  for i in range (0,int(num)):
      for j in range (0,int(num)):
        fill(140, 180)
        # Primero va la sombra
        rect(gap * (i+1) + cellsize * i + offsetX, gap * (j+1) + cellsize * j + offsetY, cellsize, cellsize)
        fill(247, 57, 57, 180)

        # Luego el rectangulo
        rect(gap * (i+1) + cellsize * i, gap * (j+1) + cellsize * j, cellsize, cellsize)
      
    
        # Para guardar el tranajo pulsa s
  if keyPressed == True and key=='s':
        saveFrame("capitulo2.jpg")
